package com.test.rss;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AsyncHttpTask.AsyncTaskInterface {

    private ListView rssListView;
    private List<RssItem> rssItems;
    private RssAdapter adapter;
    private AsyncHttpTask asyncHttpTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        asyncHttpTask = new AsyncHttpTask(this);
        rssListView = (ListView) findViewById(R.id.lv_main);
        rssItems = new ArrayList<RssItem>();
        adapter = new RssAdapter(rssItems, getApplicationContext());
        rssListView.setAdapter(adapter);

        asyncHttpTask.getRss("http://www.feedforall.com/sample.xml");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = new RssDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), "rssDialogFragment");
            }
        });
    }

    public void getRss(String url) {
        asyncHttpTask.getRss(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void doPostExecute(List<RssItem> rssItemList, int code) {
        if (code == 200) {
            adapter.setData(rssItemList);
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(getApplicationContext(), String.valueOf(code), Toast.LENGTH_SHORT).show();
        }
    }
}
