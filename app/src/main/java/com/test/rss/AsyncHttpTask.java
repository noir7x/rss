package com.test.rss;


import android.util.Log;
import android.util.Xml;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class AsyncHttpTask {

    private AsyncTaskInterface callback;
    private AsyncHttpClient client = new AsyncHttpClient();

    public interface AsyncTaskInterface {
        void doPostExecute(List<RssItem> rssItemList, int code);
    }

    public AsyncHttpTask(AsyncTaskInterface callback) {
        this.callback = callback;
    }

    public void getRss(String url) {

        Log.d("URL", url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                List<RssItem> rssItems = new ArrayList<RssItem>();
                InputStream myInputStream = new ByteArrayInputStream(responseBody);
                XmlPullParser parser = Xml.newPullParser();
                try {
                    RssItem rssItem = null;
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    parser.setInput(myInputStream, null);
                    while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
                        if (parser.getEventType() == XmlPullParser.START_TAG
                                && parser.getName().equals("item")) {
                            rssItem = new RssItem();
                        } else if (parser.getEventType() == XmlPullParser.START_TAG
                                && parser.getName().equals("title")) {
                            if (rssItem != null)
                                rssItem.setTitle(parser.nextText());
                        } else if (parser.getEventType() == XmlPullParser.START_TAG
                                && parser.getName().equals("description")) {
                            if (rssItem != null) {
                                rssItem.setDescription(parser.nextText());
                                rssItems.add(rssItem);
                            }
                        }
                        parser.next();
                    }
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                callback.doPostExecute(rssItems, statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.doPostExecute(null, statusCode);
            }


            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
