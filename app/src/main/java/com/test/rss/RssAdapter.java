package com.test.rss;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class RssAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<RssItem> rssItemList;
    private Context context;
    private int count;

    public RssAdapter(List<RssItem> rssItemList, Context context) {
        this.rssItemList = rssItemList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        count = 0;
    }

    public void setData(List<RssItem> rssItemList) {
        this.rssItemList = rssItemList;
    }

    @Override
    public int getCount() {
        return rssItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return rssItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_note, parent, false);

        ((TextView) view.findViewById(R.id.tv_date)).setText(rssItemList.get(position).getTitle());
        ((TextView) view.findViewById(R.id.tv_title)).setText(rssItemList.get(position).getDescription());

        return view;
    }
}
