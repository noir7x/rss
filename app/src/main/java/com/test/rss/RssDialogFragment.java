package com.test.rss;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

public class RssDialogFragment extends DialogFragment {

    private View view;
    private EditText editText;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.dialog_edittext, null);
        editText = (EditText) view.findViewById(R.id.ed_url);

        return new AlertDialog.Builder(getActivity())
                .setTitle("Url")
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (editText.getText().toString().trim().length() > 0)
                                    ((MainActivity) getActivity()).getRss(editText.getText().toString());

                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();
    }
}
